﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VariantApp
{
    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/covariance-contravariance/
    // Variance - 
    // Covariance preserves assignment compatibility and contravariance reverses it.
    // Variance in generic interfaces is supported for reference types only.
    class Variant
    {

        static void Test()
        {
            // Assignment compatibility.   
            string str = "test";
            // An object of a more derived type is assigned to an object of a less derived type.   
            object obj = str;

            // Covariance.   
            // Only applies to return values.
            IEnumerable<string> strings = new List<string>();
            // An object that is instantiated with a more derived type argument   
            // is assigned to an object instantiated with a less derived type argument.   
            // Assignment compatibility is preserved.   
            IEnumerable<object> objects = strings;


            // Contravariance.    
            // Only applies to arguments.
            // Assume that the following method is in the class:   
            // static void SetObject(object o) { }   
            Action<object> actObject = SetObject;
            // An object that is instantiated with a less derived type argument   
            // is assigned to an object instantiated with a more derived type argument.   
            // Assignment compatibility is reversed.   
            Action<string> actString = actObject;

            actObject(new object());    // OK
            //actString(new object());  // NOT OK
        }

        static void SetObject(object obj) { }
        static void SetString(string str) { }
        static object GetObject() { return null; }
        static string GetString() { return ""; }

        static void Test2()
        {
            // Covariance. A delegate specifies a return type as object,  
            // but you can assign a method that returns a string.  
            Func<object> del = GetString;

            // Contravariance. A delegate specifies a parameter type as string,  
            // but you can assign a method that takes an object.  
            Action<string> del2 = SetObject;
        }
    }

}
