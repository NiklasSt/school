﻿namespace VariantApp
{
    // Implementing a Variant generic interface
    // Note only interface or delegate can use out and in.
    // class SimpleClass<out R> : ICovariant<R> NOT OK
    class SimpleImpl<R> : ICovariant<R>
    {
        public R SomeMethod()
        {
            return default(R);
        }
        
        public void SetSomething(R r)
        {

        }
    }
}
