﻿using System.Collections.Generic;
using System.Text;

namespace VariantApp
{
    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/covariance-contravariance/creating-variant-generic-interfaces
    // You can declare generic type parameters in interfaces as covariant or contravariant. 

    // Covariance allow inerface methods to have more derived return types.
    // Contravariance allow inteface methods to have less derived argument types.

    // Variant - a generic interface that has covariant and contravariant generic type pramaters is called variant.

    // You declare a generic type paramter covariant by using the key word out.
    public interface ICovariant<out R>
    {
        R SomeMethod();

        // The following statement is not allowed
        //void SetSomething(R r);
    }

    // You declare a generic type paramter contravariant by using the key word in.
    interface IContravariance<in R>
    {
        void DoSomething(R r);
        void DoSomething<T>() where T : R;

        // The following statement is not allowed
        //R GetSomething();
    }

    interface IVariant<in T, out R>
    {
        void DoSomething(T t);
        void DoSomething<U>() where U : T;
        R GetSomething<Q>(Q q) where Q : T;
    }

    // This will not work because ICovariant<T> is covariant.
    //interface ICoContravariant<in T> : ICovariant<T> { } 

    interface IInvariant<T> : ICovariant<T>, IContravariance<T> { }

    interface IContra <in T> { void Print(T t); }
}
