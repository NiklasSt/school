﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VariantApp
{
    class Contra <T> : IContra<T>
    {
        public void Print(T t)
        {
            Console.WriteLine($"t is of type {t.GetType()} and t = {t.ToString()}");
        }
    }

    class ContraTest
    {
        // Contravariant is that you can use more less derived parameters in the created objcect
        // than the declared varable is defined to use.
        IContra<string> b = new Contra<object>();
        // b.Print(new Object()); is not ok as normal. But look at the created object on the line above, 
        // it takes a less derived type than the IContra declaration IContra<string> b. This is contravariant.
        // b can allow to use objects with less derived types and force it to constrain to the more derived type.

        // This is also possibe
        IContra<Program> bi = new Contra<object>();

        public void TestMethod()
        {
            b.Print("Hello");

            bi.Print(new Program());

            // Note that a var declaraiotion does not really give anything extra here.
            // var will create only say that c is of type Contra<string> when you
            // really want at least an interface. At least you like to use the interaface IContra<string>.
            var c = new Contra<string>();
        }
    }
}
