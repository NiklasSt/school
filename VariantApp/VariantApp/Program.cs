﻿using System;

namespace VariantApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new AnotherImpl();
            a.TesMethod();

            var b = new ContraTest();
            b.TestMethod();

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }
    }
}
