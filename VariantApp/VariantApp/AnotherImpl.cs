﻿using System;

namespace VariantApp
{
    class AnotherImpl
    {
        ICovariant<string> istr = new SimpleImpl<string>();
        ICovariant<object> iobj = new SimpleImpl<string>();

        // But you cant do this:
        SimpleImpl<string> s = new SimpleImpl<string>();

        // The interface is covariant.  
        static ICovariant<string> ibutton = new SampleImplementation<string>();
        static ICovariant<Object> ioibj = ibutton;

        // The class is invariant.  
        SampleImplementation<string> s2 = new SampleImplementation<string>();
        // The following statement generates a compiler error  
        // because classes are invariant.  
        // SampleImplementation<Object> obj = button;  

        public void TesMethod()
        {
            s2.GetSomething();
            s2.SetSomething("hello");
        }

    }
}
