﻿using System;

namespace VariantApp
{
    internal class SampleImplementation<R> : ICovariant<R>
    {
        public R GetSomething()
        {
            // Some code.  
            return default(R);
        }

        // Note that SetSomething(R r) is not part of the interface ICovariant<R>
        public void SetSomething(R r)
        {
            Console.WriteLine(r);
        }

        public R SomeMethod()
        {
            return default(R);
        }
    }
}
