﻿using AutoMapper;
using AutomapperApp.Model;
using AutomapperApp.Model.Dto;
using System;

namespace AutomapperApp
{

    // http://automapper.org/
    class Program
    {
        static void Main(string[] args)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Order, SummaryDto>()
                //        set OrderDto.OrderName, get Order.Name
                .ForMember(d => d.OrderName, (o) => o.MapFrom(d => d.Name))
                .ForMember(d => d.ProductPrice, o => o.MapFrom(d => d.Orders[0].Price));
            });

            var order = new Order() { Name = "Cool Order", Id = "kjlkjjj", Customer = new Customer() { } };
            order.Orders.Add(new Product { Name = "Cool Product", Price = 20.6F });

            var summaryDto = Mapper.Map<Order, SummaryDto>(order);
            Console.WriteLine($"Order Name = {summaryDto.OrderName}, Price = {summaryDto.ProductPrice}");
            Console.ReadKey();
        }
    }
}
