﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomapperApp.Model.Dto
{
    class SummaryDto
    {
        public string OrderName { get; set; }
        public float ProductPrice { get; set; }

    }
}
