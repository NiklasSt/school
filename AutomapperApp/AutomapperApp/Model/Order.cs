﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomapperApp.Model
{
    class Order
    {
        public string Id { get; set; }
        public IList<Product> Orders { get; set; } = new List<Product>();
        public Customer Customer { get; set; }

        public string Name { get; set; }

    }
}
