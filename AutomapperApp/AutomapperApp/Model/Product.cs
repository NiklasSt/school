﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomapperApp.Model
{
    class Product
    {
        public string  Name { get; set; }
        public float Price { get; set; }
    }
}
