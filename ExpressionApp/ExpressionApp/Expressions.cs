﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ExpressionApp
{
    class Expressions
    {
        public delegate R Funt<in T, in V, out R>(T t, V v);
        public delegate R plus<in T, in V, out R>(T t, V v);
        public delegate int plu(int i, int k);
        public void Test()
        {
            plus<int, int, int> p = (x, y) => x + y;
            plu p2 = (x, y) => x + y;
            Funt<int, int, int> p3 = (x, y) => x + y;

            // Will not work because not same type.
            // p = p2; 

            // Will not work because not same type ?
            // p = p3;

            Expression<plus<int, int, int>> me = (x, y) => x + y;

            Console.WriteLine($"NodeType: {me.NodeType}");

            var body = me.Body;
            Console.WriteLine($"Body: {body}");

            var returnType = me.ReturnType;
            Console.WriteLine($"Return type: {returnType}");

            var param = me.Parameters;
            var param0 = param[0];
            Console.WriteLine($"{param0}, NodeType is {param0.NodeType}, Type is {param0.Type}");
            var param1 = param[1];
            Console.WriteLine($"{param1}, NodeType is {param1.NodeType}, Type is {param1.Type}");

            Console.WriteLine($"NodeType is {body.NodeType}");

            Console.WriteLine($"Result is: {me.Compile()(2,3)}");
        }
    }
}
